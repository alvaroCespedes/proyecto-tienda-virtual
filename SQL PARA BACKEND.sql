/*============ base de datos se tiene que llamar bd_tienda ============*/

create table categorias
(
   id_categoria         bigint not null auto_increment  comment '',
   nombre_categoria     varchar(100) not null  comment '',
   primary key (id_categoria)
);

/*==============================================================*/
/* Table: productos                                             */
/*==============================================================*/
create table productos
(
   id_producto          bigint not null auto_increment  comment '',
   id_categoria         bigint not null  comment '',
   nombre_producto      varchar(100) not null  comment '',
   referencia_producto  varchar(20) not null  comment '',
   precio_costo_producto numeric(12,2) not null  comment '',
   precio_venta_producto numeric(12,2) not null  comment '',
   cantidad_producto    float not null  comment '',
   primary key (id_producto)
);

alter table productos add constraint fk_producto_reference_categori foreign key (id_categoria)
      references categorias (id_categoria) on delete cascade on update cascade;

create table proveedores
(
   id_proveedor         bigint not null auto_increment  comment '',
   nombre_proveedor     varchar(100) not null  comment '',
   direccion_proveedor     varchar(100) not null  comment '',
   telefono_proveedor     varchar(10) not null  comment '',
   primary key (id_proveedor)
);


INSERT INTO categorias (nombre_categoria) VALUES ('LACTEOS');
INSERT INTO categorias (nombre_categoria) VALUES ('CARNICOS');
INSERT INTO categorias (nombre_categoria) VALUES ('ASEO');
INSERT INTO categorias (nombre_categoria) VALUES ('HOGAR');
INSERT INTO categorias (nombre_categoria) VALUES ('ELECTRODOMESTICOS');

INSERT INTO `bd_tienda`.`productos` (`id_categoria`, `nombre_producto`, `referencia_producto`, `precio_costo_producto`, `precio_venta_producto`, `cantidad_producto`) 
VALUES ('1', 'YOGURTH', 'YOG', '2000', '2500', '50');
INSERT INTO `bd_tienda`.`productos` (`id_categoria`, `nombre_producto`, `referencia_producto`, `precio_costo_producto`, `precio_venta_producto`, `cantidad_producto`) 
VALUES ('1', 'ALPINITO', 'ALP', '4000', '5000', '30');
INSERT INTO `bd_tienda`.`productos` (`id_categoria`, `nombre_producto`, `referencia_producto`, `precio_costo_producto`, `precio_venta_producto`, `cantidad_producto`) 
VALUES ('3', 'FABULOSO', 'FAB', '5000', '6000', '40');

INSERT INTO `bd_tienda`.`proveedores` (`nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`) VALUES ('LACTEOS BOYACA', 'CRA 1 - 17 -08', '3104502030');
INSERT INTO `bd_tienda`.`proveedores` (`nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`) VALUES ('CARNICOS DEL LLANO', 'CALLE 40 - 12 -34', '3157845211');
INSERT INTO `bd_tienda`.`proveedores` (`nombre_proveedor`, `direccion_proveedor`, `telefono_proveedor`) VALUES ('DISTRIASEOS', 'TV 45 - 12 - 33', '3207985421');






