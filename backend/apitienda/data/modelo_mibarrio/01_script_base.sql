/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     5/09/2022 10:22:41 a. m.                     */
/*==============================================================*/


/*==============================================================*/
/* Table: categorias                                            */
/*==============================================================*/
create table categorias
(
   id_categoria         bigint not null auto_increment  comment '',
   nombre_categoria     varchar(100) not null  comment '',
   primary key (id_categoria)
);

/*==============================================================*/
/* Table: productos                                             */
/*==============================================================*/
create table productos
(
   id_producto          bigint not null auto_increment  comment '',
   id_categoria         bigint not null  comment '',
   nombre_producto      varchar(100) not null  comment '',
   referencia_producto  varchar(20) not null  comment '',
   precio_costo_producto numeric(12,2) not null  comment '',
   precio_venta_producto numeric(12,2) not null  comment '',
   cantidad_producto    float not null  comment '',
   primary key (id_producto)
);

alter table productos add constraint fk_producto_reference_categori foreign key (id_categoria)
      references categorias (id_categoria) on delete cascade on update cascade;

