package com.misiontic.apitienda.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.misiontic.apitienda.entidades.Producto;

@Repository
public interface ProductoRepositorio extends JpaRepository<Producto, Long> {

}
