package com.misiontic.apitienda.interfaces;

import java.util.List;

public interface Operaciones<T> {

	public abstract boolean agregar(T objeto);

	public abstract List<T> consultar();

	public abstract Integer cantidadRegistros();

	public abstract boolean eliminar(Long codigo);

	public abstract boolean actualizar(T objeto);

	public abstract T buscar(Long codigo);

}
