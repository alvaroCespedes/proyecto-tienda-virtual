package com.misiontic.apitienda.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.misiontic.apitienda.entidades.Categoria;

@Repository
public interface CategoriaRepositorio extends JpaRepository<Categoria, Long> {

	@Query("SELECT c FROM Categoria c")
	public List<Categoria> obtenerCategorias();
	
	@Query("SELECT c FROM Categoria c WHERE c.idCategoria = :codigo")
    public Categoria obtenerUno(Long codigo);

}
