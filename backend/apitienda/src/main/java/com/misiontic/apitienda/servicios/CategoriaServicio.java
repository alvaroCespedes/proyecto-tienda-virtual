package com.misiontic.apitienda.servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.misiontic.apitienda.entidades.Categoria;
import com.misiontic.apitienda.interfaces.Operaciones;
import com.misiontic.apitienda.repositorios.CategoriaRepositorio;

@Service
public class CategoriaServicio implements Operaciones<Categoria> {

	@Autowired
	private CategoriaRepositorio cateRepo;

	@Override
	public boolean agregar(Categoria objeto) {
		Categoria objTemporal = cateRepo.save(objeto);
		return objTemporal != null;
	}

	@Override
	public List<Categoria> consultar() {
		return cateRepo.findAll();
	}

	@Override
	public Integer cantidadRegistros() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean eliminar(Long codigo) {
		cateRepo.deleteById(codigo);
		return !cateRepo.existsById(codigo);
	}

	@Override
	public boolean actualizar(Categoria objeto) {
		Optional<Categoria> objetoVerificado = cateRepo.findById(objeto.getIdCategoria());
		if (!objetoVerificado.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Registro no existe.");
		} else {
			cateRepo.save(objeto);
			return true;
		}
	}

	@Override
	public Categoria buscar(Long codigo) {
		return cateRepo.obtenerUno(codigo);
	}

}
