const url = 'http://localhost:8080/api/productos';
const contenedor = document.querySelector('tbody');
let resultados = '';

const modalProducto = new bootstrap.Modal(document.getElementById('modalProductos'));
const formulario = document.querySelector('form');
let opcion = '';

const cajaNombreProducto = document.getElementById('nombre');
btnCrear.addEventListener('click', () => {
    cajaNombreProducto.value = '';
    modalProducto.show();
    opcion = 'crear';
});

const mostrarRegistros = (arregloProductos) => {
    arregloProductos.forEach(producto => {
        resultados += `<tr>
                            <td>${producto.idProducto}</td>
                            <td>${producto.nombreProducto}</td>
                            <td>${producto.referenciaProducto}</td>
                            <td>${producto.precioCostoProducto}</td>
                            <td>${producto.precioVentaProducto}</td>
                            <td class="text-center">
                                <a class="btnEditar btn btn-primary">Editar</a> 
                                <a class="btnBorrar btn btn-danger">Borrar</a> 
                            </td>
                       </tr>
                       `
    });
    contenedor.innerHTML = resultados;
}


//mostrar todos los registros de la tabla productos
fetch(url + '/listado')
    .then(response => response.json())
    .then(data => mostrarRegistros(data))
    .catch(error => console.log(error))

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector)) {
            handler(e)
        }
    })
}

//procedimiento borrar una producto
on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode;
    const id = fila.firstElementChild.innerHTML;

    alertify.confirm("¿Desea eliminar el registro.?",
        function () {
            const urlBorrar = url + '/borrar/' + id;
            fetch(urlBorrar, {
                method: 'DELETE'
            })
                .then(response => response.json())
                .then(() => location.reload())
            alertify.success('Ok');
        },
        function () {
            alertify.error('Error no fue eliminado el registro.');
        });
})

//proceso para editar una producto
let ifForm = 0;
on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode;
    ifForm = fila.children[0].innerHTML;
    const nombreProducto = fila.children[1].innerHTML;
    cajaNombreProducto.value = nombreProducto;
    opcion = 'editar';
    modalProducto.show();
})

//proceso para consumir el servicio de editar o crear
formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    if (opcion == 'crear') {
        const urlCrear = url + '/crear';
        fetch(urlCrear, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                nombreProducto: cajaNombreProducto.value
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevaProducto = [];
                nuevaProducto.push(data);
                mostrarRegistros(nuevaProducto);
                if (data.idProducto > 0) {
                    alertify.success('Registro creado correctamente.');
                } else {
                    alertify.error('Registro NO fue creado correctamente.');
                }
            })
    }

    if (opcion == 'editar') {
        const urlEditar = url + '/actualizar';
        fetch(urlEditar, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idProducto: ifForm,
                nombreProducto: cajaNombreProducto.value
            })
        })
            .then(response => response.json())
            .then(response => location.reload())
            alertify.success('Registro editado correctamente.');
    }
    modalProducto.hide();
})