const url = 'http://localhost:8080/api/categorias';
const contenedor = document.querySelector('tbody');
let resultados = '';

const modalCategoria = new bootstrap.Modal(document.getElementById('modalCategorias'));
const formulario = document.querySelector('form');
let opcion = '';

const cajaNombreCategoria = document.getElementById('nombre');
btnCrear.addEventListener('click', () => {
    cajaNombreCategoria.value = '';
    modalCategoria.show();
    opcion = 'crear';
});

const mostrarRegistros = (arregloCategorias) => {
    arregloCategorias.forEach(categoria => {
        resultados += `<tr>
                            <td>${categoria.idCategoria}</td>
                            <td>${categoria.nombreCategoria}</td>
                            <td class="text-center">
                                <a class="btnEditar btn btn-primary">Editar</a> 
                                <a class="btnBorrar btn btn-danger">Borrar</a> 
                            </td>
                       </tr>
                       `
    });
    contenedor.innerHTML = resultados;
}


//mostrar todos los registros de la tabla categorias
fetch(url + '/todas')
    .then(response => response.json())
    .then(data => mostrarRegistros(data))
    .catch(error => console.log(error))

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector)) {
            handler(e)
        }
    })
}

//procedimiento borrar una categoria
on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode;
    const id = fila.firstElementChild.innerHTML;

    alertify.confirm("¿Desea eliminar el registro.?",
        function () {
            const urlBorrar = url + '/borrar/' + id;
            fetch(urlBorrar, {
                method: 'DELETE'
            })
                .then(response => response.json())
                .then(() => location.reload())
            alertify.success('Ok');
        },
        function () {
            alertify.error('Error no fue eliminado el registro.');
        });
})

//proceso para editar una categoria
let ifForm = 0;
on(document, 'click', '.btnEditar', e => {
    const fila = e.target.parentNode.parentNode;
    ifForm = fila.children[0].innerHTML;
    const nombreCategoria = fila.children[1].innerHTML;
    cajaNombreCategoria.value = nombreCategoria;
    opcion = 'editar';
    modalCategoria.show();
})

//proceso para consumir el servicio de editar o crear
formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    if (opcion == 'crear') {
        const urlCrear = url + '/crear';
        fetch(urlCrear, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                nombreCategoria: cajaNombreCategoria.value
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevaCategoria = [];
                nuevaCategoria.push(data);
                mostrarRegistros(nuevaCategoria);
                if (data.idCategoria > 0) {
                    alertify.success('Registro creado correctamente.');
                } else {
                    alertify.error('Registro NO fue creado correctamente.');
                }
            })
    }

    if (opcion == 'editar') {
        const urlEditar = url + '/actualizar';
        fetch(urlEditar, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idCategoria: ifForm,
                nombreCategoria: cajaNombreCategoria.value
            })
        })
            .then(response => response.json())
            .then(response => location.reload())
            alertify.success('Registro editado correctamente.');
    }
    modalCategoria.hide();
})